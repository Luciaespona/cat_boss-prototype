# encoding=utf8  
 
import cat_util

def get_token_variations(token, add_plurals = True):
    
    variations = []
    
    # singular and plural
    if token[-2:] == 'ES':
        #variations += [ token[:-2], token[:-1] ]
        variations += [ token[:-2] ]
        if add_plurals:
            variations += [ token ]
    elif token[-1] == 'S':
        variations += [ token[:-1] ]
        if add_plurals:
            variations += [ token ]
    else:
        if add_plurals:
            if token[-1] in 'AEIOU':
               variations += [ token+'S']
            else:
               variations += [ token+'ES' ]
        variations += [ token ]

    return variations

def token_is_stopword(token, stopwords):
    if token in stopwords:
        return True
    for variation in get_token_variations(token, add_plurals = True):
        if variation in stopwords:
            return True
    return False

def tokenize(title, stopwords = []):
    
    # unicode 
    unicode_title =  unicode(title.strip())
    
    # remove accents (TODO: think what to do about Ñ)
    unaccented_title = cat_util.strip_accents(unicode_title)

    # upper
    unaccented_title = unaccented_title.upper()
    
    # split
    title_raw_tokens = unaccented_title.split()
    
    # remove empty
    title_tokens_non_empty = filter(None, title_raw_tokens)
    
    # remove stopwords and 1 letter
    title_tokens = []
    for token in title_tokens_non_empty:
        if not token_is_stopword(token, stopwords) and len(token)>1:
            title_tokens += [token]

    # variations
    variations = []
    for token in title_tokens:
        # remove only numbers:
        if not token.isdigit():
            # don't variate on words with numbers
            if cat_util.has_numbers(token):
                variations += [ token ]
            else:
                # omit short words and words with no vowels
                vowels = ['A','E','I','O','U']
                if len(token)<=2 or not any(char in vowels for char in token):
                    variations += [ token ]
                else:
                    variations += get_token_variations(token, add_plurals = False)
                    
    
    # decide wether to remove plurals
    #title_tokens += variations
    title_tokens = variations
    
    # remove duplicates
    title_tokens = list(set(title_tokens))
    
    return title_tokens