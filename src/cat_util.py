# encoding=utf8  

#import unidecode
import unicodedata
import string

import sys
reload(sys)
sys.setdefaultencoding('utf8')

# alternative 
# unaccented_title = unidecode.unidecode(unicode_title)

def robust_decode(bs):
    '''Takes a byte string as param and convert it into a unicode one.
First tries UTF8, and fallback to Latin1 if it fails'''
    cr = None
    try:
        cr = bs.decode('utf8')
    except UnicodeDecodeError:
        cr = bs.decode('latin1')
    return cr

def strip_accents(text):

    text = text.upper().replace('Ñ', 'NH')  
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    
    # remove punctuation
    text = text.replace('.', " ")
    text = text.replace('-', "")
    text = text.replace(',', " ")
    text = text.replace('¿', "")
    text = text.replace('\t', " ")
    text = text.replace('\r', " ")
    text = text.replace('\n', " ")
    
    for symbol in string.punctuation + "?":
        text = text.replace(symbol, "")

    return str(text.decode('utf8'))

def has_numbers(inputString):
    return any(char.isdigit() for char in inputString)