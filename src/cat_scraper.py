import sys, getopt
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup

def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200 
            and content_type is not None 
            and content_type.find('html') > -1)


def log_error(e):
    """
    It is always a good idea to log errors. 
    This function just prints them, but you can
    make it do anything.
    """
    print(e)

# -----------------
def scrap_url(url):
    raw_html = simple_get(url)
    return raw_html    

def scrap_file(file):
    raw_html = open(file).read()
    html = BeautifulSoup(raw_html, 'html.parser')
    for a in html.select('a'):
        if a.get('class',[''])[0] == 'cat1':
            #print(a['class'])    
            print(a.get('href','').upper())    
        elif a.get('class',[''])[0] == 'cat2':
            #print(a['class'])    
            print(a['href'])    

# ---------
def usage():
    print 'Usage: cat_scraper.py -u <url>'

def main(argv):

    url = ''
    file = ''
    try:
        opts, args = getopt.getopt(argv,"hf:",["file=", "help"])
    except getopt.GetoptError as e:
        print(e)
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-u", "--url"):
            url = arg.strip()
        elif opt in ("-f", "--file"):
            file = arg.strip()
        else:
            usage()
            sys.exit(2)
    if url:
        print (' - url is "' + url + '"')
        scrap_url(url)
    elif file:
        print (' - file is "' + file + '"')
        scrap_file(file)
    else:
        usage()

    
if __name__ == "__main__":
    main(sys.argv[1:])