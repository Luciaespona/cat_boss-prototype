# encoding=utf8  

import cat_tokenizer
import cat_ignore
import sys
import collections

CAT_FILE_PATH = '../data/sections_categories.csv'
CUSTOM_TOKEN_FILE_PATH = '../data/test_token_categories.csv'
MA_FILE_PATH = '../data/cat_scraper_data/output/milanuncios_scrap.csv'
MAX_SHARE_SEC_TOKEN = 3
MAX_CAT_SEC_TOKEN = 3

OUTPUT_FILE = '../data/cat_miner_output/miner_output.csv'

def add_to_tokens_dict(token_dict, section, category):
    if not token_dict:
        token_dict = {'sections':{}, 'categories':{}}
    if not token_dict['sections'].get(section, False):
        token_dict['sections'][section] = 0
    token_dict['sections'][section] += 1
    
    if len(category)>0:
        category_key = section + '\t' + category
        if not token_dict['categories'].get(category_key, False):
            token_dict['categories'][category_key] = 0
        token_dict['categories'][category_key] += 1
    return token_dict

def main():
    stopwords = cat_ignore.get_stopwords(scrap = True)

    all_tokens_dict = collections.OrderedDict()
    # hand made tokens
    with open(CUSTOM_TOKEN_FILE_PATH) as f:
        for line in f.readlines():
            if len(line.strip())>0:             
                token = unicode(line.strip().split()[0].upper())
                section = line.strip().split()[1].upper()
                try:
                    category = line.strip().split()[2].upper()
                except:
                    category = ""
            all_tokens_dict[token] = add_to_tokens_dict(all_tokens_dict.get(token, False), section, category)

    # milanuncios tokens
    count = 0
    line_num = 0
    with open(MA_FILE_PATH) as f:
        for line in f.readlines():
            line_num += 1
            if len(line.strip())>0:             
                category_text = unicode(line.strip().split('\t')[0].upper())
                try:
                    text = line.strip().split('\t')[1].upper()
                except:
                    print 'ERROR'
                    print text
                    print line_num
                    sys.exit(0)
                
                count += len(text.split())
                section = category_text.split('.')[0].upper()
                
                if len(category_text.split('.'))>1:
                    category = category_text.split('.')[1].upper()
                else:
                    category = ""
                
                tokens = cat_tokenizer.tokenize(text, stopwords=stopwords)
                for token in  tokens:
                    all_tokens_dict[token] = add_to_tokens_dict(all_tokens_dict.get(token, False), section, category)
                    
    removed_tokens = set()
    final_dict = {}
    for key,value in all_tokens_dict.iteritems():
        if key not in stopwords and len(key)>1:
            sections = value['sections']
            categories = value['categories']
            if len(sections.keys())<MAX_SHARE_SEC_TOKEN:
                final_dict[key] = {'sections':sections.keys(), 'categories':categories.keys()}
            else: 
                #select first two sections
                final_sections = []
                sorted_scores = sections.values()
                sorted_scores.sort(reverse=True)
                for score in sorted_scores:
                    selected_sections = []
                    for section,hits in sections.iteritems():
                        if hits == score:
                            selected_sections += [section]
                    if len(selected_sections)+len(final_sections)>=MAX_SHARE_SEC_TOKEN:
                        break
                    else:
                        final_sections += selected_sections
                if final_sections:
                    selected_categories = {}
                    final_dict[key] = {'sections':final_sections, 'categories':[]}
                    for category, hits in value['categories'].iteritems():
                        section_category = category.split('\t')[0]
                        if section_category in final_sections:
                            if section_category not in selected_categories:
                                selected_categories[section_category] = {}                    
                            selected_categories[section_category][category] = hits
                        
                    for section_category, categories in selected_categories.iteritems():
                        cats_in_sec = 0
                        prev_hits = 0
                        for category in sorted(categories, key=categories.get, reverse=True):
                            cats_in_sec += 1
                            hits = categories[category]
                            final_dict[key]['categories'] += [category]
                            if cats_in_sec >= MAX_CAT_SEC_TOKEN and hits<prev_hits:                                
                                break
                            else:
                                prev_hits = hits
                else:
                    removed_tokens.add(key)
    
    with open(OUTPUT_FILE, "w") as output:
        for key,value in final_dict.iteritems():
            for section in final_dict[key]['sections']:
                output.write("\t".join([key, section]) + '\n')
            for category in final_dict[key]['categories']:
                output.write("\t".join([key, category]) + '\n') 

    print count
    print len(all_tokens_dict.keys())
    print (float(len(all_tokens_dict.keys()))/float(count))

    list_removed_tokens = list(removed_tokens)
    list_removed_tokens.sort()
    print 'Ignored: ' + '\t'.join(list_removed_tokens) + ', total ' + str( len(list_removed_tokens))
    
if __name__ == "__main__":
    main()