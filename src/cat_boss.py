# encoding=utf8  

import getopt
import collections

import cat_data
import cat_tokenizer
import cat_ignore

import sys  

reload(sys)  
sys.setdefaultencoding('utf8')

def evaluate_results(raw_results):
    sections = {}
    categories = {}
    
    for key,value in raw_results.iteritems():

        if len(value)>0:
            for result in value:
                id, token, section, category, score = result
            
                if not sections.get(section, False):
                    sections[section] = []
                sections[section] += [ token ]
        
                if category:
                    category_key = section +'.'+ category
                    if not categories.get(category_key, False):
                        categories[category_key] = []
                    categories[category_key] += [ token ]
    
    # compress
    for section,tokens in sections.iteritems():
        sections[section] = list(set(tokens))
    
    for category,tokens in categories.iteritems():
        categories[category] = list(set(tokens))
        
    # sort
    final_sections = collections.OrderedDict()
    for key, value in sorted(sections.iteritems(), key=lambda (k,v): (len(v),k), reverse=True):
        final_sections[key] = value
    
    final_categories = collections.OrderedDict()
    for section in final_sections.keys():
        for category, value in sorted(categories.iteritems(), key=lambda (k,v): (len(v),k), reverse=True):
            section_category = category.split('.')[0]
            if section_category == section:
                final_categories[category] = categories[category]

    return (final_sections, final_categories)

def print_if_verbose(verbose, text):
    if verbose:
        print text
    
def suggest_category(title, verbose = True, db = None):
  
    
    print_if_verbose(verbose, "Hi, I am the cat boss >^.^< ")
    
    if db:
        print_if_verbose(verbose,  "\n 1. Existing DB")
        cat_db = db
    else:
        print_if_verbose(verbose,  "\n 1. Init DB")
        cat_db = cat_data.init_cat_db()
    
    print_if_verbose(verbose,  "\n 2. Get STOPWORDS")
    stopwords = cat_ignore.get_stopwords()
    print_if_verbose(verbose,  " - got " + str(len(stopwords)) )
    
    print_if_verbose(verbose,  "\n 3. TOKENIZE title")
    tokens = cat_tokenizer.tokenize(title, stopwords)
    print_if_verbose(verbose, (tokens))
    
    print_if_verbose(verbose,  "\n 4. QUERY tokens")
    raw_results = cat_data.query_tokens(cat_db, tokens)
    print_if_verbose(verbose,  raw_results)
    
    print_if_verbose(verbose,  "\n 5. Evaluate RESULTS: ")
    sections, categories = evaluate_results(raw_results)
    
    if verbose:
        print "\n\t SECTIONS:"
        for section, tokens in sections.iteritems():
            print '\t - ' + section + ': ' + ', '.join(tokens).lower()
    
        print "\n\t CATEGORIES:"
        for category, tokens in categories.iteritems():
            print '\t - ' + category + ': ' + ', '.join(tokens).lower()
    
    if not db:
        print_if_verbose(verbose,  " -> Close DB")
        cat_data.close_db(cat_db)
    
    return (sections, categories)
    
def usage():
    print 'Usage: cat_boss.py -t "<title>"'

def main(argv):

    title = ''
    try:
        opts, args = getopt.getopt(argv,"ht:",["title=", "help"])
    except getopt.GetoptError as e:
        print(e)
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-t", "--title"):
            title = arg.strip()
        else:
            usage()
            sys.exit(2)
    if title:
        print (' - title is "' + title + '"')
        suggest_category(title)
    else:
        usage()

    
if __name__ == "__main__":
    main(sys.argv[1:])
