# encoding=utf8  

import sys, getopt
import time
import collections

from bs4 import BeautifulSoup

from cat_util import strip_accents, robust_decode

BASE_PATH = '../data/cat_scraper_data/vibbo/'
#OUTPUT_FILE = '../data/cat_scraper_data/output/vibbo_scrap_titles_only.csv'
OUTPUT_FILE = '../data/cat_scraper_data/output/vibbo_scrap.csv'

reload(sys)  
sys.setdefaultencoding('utf8')

CAT_MAP = collections.OrderedDict()
CAT_MAP ['telefonos-moviles-de-segunda-mano-toda-espana'] = 'ELECTRONIC_AND_COMPUTERS.SMARTPHONE'
CAT_MAP ['portatiles-netbooks-de-segunda-mano-toda-espana-02'] = 'ELECTRONIC_AND_COMPUTERS.LAPTOPS'
CAT_MAP ['ordenadores-de-segunda-mano-toda-espana-02'] = 'ELECTRONIC_AND_COMPUTERS.DESKTOPS'
CAT_MAP ['tablets-readers-de-segunda-mano-toda-espana'] = 'ELECTRONIC_AND_COMPUTERS.TABLETS'
CAT_MAP ['impresora'] = 'ELECTRONIC_AND_COMPUTERS.PRINT_AND_SCAN'
CAT_MAP ['fotocopiadora'] = 'ELECTRONIC_AND_COMPUTERS.PRINT_AND_SCAN'
CAT_MAP ['raton'] = 'ELECTRONIC_AND_COMPUTERS.ELECTRONIC_ACCESSORIES'
CAT_MAP ['consolas'] = 'ELECTRONIC_AND_COMPUTERS.CONSOLES'
CAT_MAP ['videojuegos-de-segunda-mano-toda-espana-profesionales'] = 'ELECTRONIC_AND_COMPUTERS.VIDEOGAMES'
CAT_MAP ['router'] = 'ELECTRONIC_AND_COMPUTERS.NETWORKING'
CAT_MAP ['switch'] = 'ELECTRONIC_AND_COMPUTERS.NETWORKING'
CAT_MAP ['dron'] = 'ELECTRONIC_AND_COMPUTERS.DRONES'
 
CAT_MAP ['video-camaras-de-segunda-mano-toda-espana-camara'] = 'AUDIO_VISUAL.FILM'
CAT_MAP ['camaras-accesorios-fotografia-de-segunda-mano-toda-espana'] = 'AUDIO_VISUAL.PHOTOGRAPHY'
CAT_MAP ['sonido-toda-espana'] = 'AUDIO_VISUAL.AUDIO_AND_DJ_EQUIPMENT'
CAT_MAP ['proyectores-de-segunda-mano-toda-espana'] = 'AUDIO_VISUAL.PROJECTORS'
CAT_MAP ['televisores-de-segunda-mano-toda-espana'] = 'AUDIO_VISUAL.SCREENS'

CAT_MAP ['instrumentos-musicales-de-segunda-mano-toda-espana'] = 'ARTS_AND_EDUCATION.MUSICAL_INSTRUMENTS'
# CAT_MAP ['formacion'] = 'ARTS_AND_EDUCATION.LEARNING_MATERIAL'
# CAT_MAP ['pinturas'] = 'ARTS_AND_EDUCATION.ART_AND_CRAFTS'
# CAT_MAP ['esculturas'] = 'ARTS_AND_EDUCATION.ART_AND_CRAFTS'
# CAT_MAP ['otros-articulos-de-arte'] = 'ARTS_AND_EDUCATION.ART_AND_CRAFTS'
# CAT_MAP ['libros'] = 'ARTS_AND_EDUCATION.CINEMA_MUSIC_AND_LITERATURE'
# CAT_MAP ['peliculas'] = 'ARTS_AND_EDUCATION.CINEMA_MUSIC_AND_LITERATURE'
# CAT_MAP ['cds-cintas-y-vinilos'] = 'ARTS_AND_EDUCATION.CINEMA_MUSIC_AND_LITERATURE'
CAT_MAP ['cd'] = 'ARTS_AND_EDUCATION.CINEMA_MUSIC_AND_LITERATURE'

CAT_MAP ['cunas-de-segunda-mano-toda-espana'] = 'BABYS_AND_KIDS.BABY_CARE'
# CAT_MAP ['tronas'] = 'BABYS_AND_KIDS.BABY_CARE'
# CAT_MAP ['baneras-cambiadores'] = 'BABYS_AND_KIDS.BABY_CARE'
# CAT_MAP ['accesorios-para-bebe'] = 'BABYS_AND_KIDS.BABY_CARE'
# CAT_MAP ['hamacas-columpios'] = 'BABYS_AND_KIDS.BABY_CARE'
# 
# CAT_MAP ['mochilas-fulares-portabebes'] = 'BABYS_AND_KIDS.PRAMS_AND_CARRIERS'
# CAT_MAP ['coches-de-bebe'] = 'BABYS_AND_KIDS.PRAMS_AND_CARRIERS'
# CAT_MAP ['silla-infantil'] = 'BABYS_AND_KIDS.CAR_AND_BIKE_SEATS'
CAT_MAP ['juguetes-de-segunda-mano-toda-espana'] = 'BABYS_AND_KIDS.TOYS_AND_GAMES'
# 
CAT_MAP ['disfraz'] = 'PARTY_AND_EVENTS.PARTY_COSTUMES'
# CAT_MAP ['disfraces-hombre'] = 'PARTY_AND_EVENTS.PARTY_COSTUMES'
# CAT_MAP ['disfraces-nino'] = 'PARTY_AND_EVENTS.PARTY_COSTUMES'
CAT_MAP ['fiestas'] = 'PARTY_AND_EVENTS.PARTY_DECORATION'
# #CAT_MAP [''] = 'PARTY_AND_EVENTS.PARTY_ATTRACTIONS'
# #CAT_MAP [''] = 'PARTY_AND_EVENTS.PARTY_FURNITURE'
# CAT_MAP ['celebraciones-local'] = 'PARTY_AND_EVENTS.PARTY_SPACES'
 
CAT_MAP ['piscina'] = 'HOME_AND_GARDEN.POOL'
CAT_MAP ['jardin'] = 'HOME_AND_GARDEN.GARDEN'
# CAT_MAP ['plantas'] = 'HOME_AND_GARDEN.GARDEN'
# CAT_MAP ['mobiliario-de-jardin'] = 'HOME_AND_GARDEN.GARDEN'
# CAT_MAP ['electrodomesticos'] = 'HOME_AND_GARDEN.ELECTRICAL_HOUSEHOLD_APPLIANCE'
# CAT_MAP ['iluminacion'] = 'HOME_AND_GARDEN.ILLUMINATION'
# CAT_MAP ['arte-y-decoracion'] = 'HOME_AND_GARDEN.DECORATION'
# CAT_MAP ['articulos-textiles'] = 'HOME_AND_GARDEN.DECORATION'
# CAT_MAP ['menaje-del-hogar'] = 'HOME_AND_GARDEN.DECORATION'
# CAT_MAP ['antiguedades-de-coleccion'] = 'HOME_AND_GARDEN.DECORATION'
# CAT_MAP ['velas'] = 'HOME_AND_GARDEN.DECORATION'
# CAT_MAP ['muebles'] = 'HOME_AND_GARDEN.FURNITURE'
# CAT_MAP ['muebles-02'] = 'HOME_AND_GARDEN.FURNITURE'
# CAT_MAP ['muebles-03'] = 'HOME_AND_GARDEN.FURNITURE'
# CAT_MAP ['accesorios-productos-perros'] = 'HOME_AND_GARDEN.PETS_SUPPLIES'
# CAT_MAP ['accesorios-productos-gatos'] = 'HOME_AND_GARDEN.PETS_SUPPLIES'
# CAT_MAP ['accesorios-productos-pajaros'] = 'HOME_AND_GARDEN.PETS_SUPPLIES'
# 
# CAT_MAP ['kayak'] = 'SPORTS_AND_ADVENTURE.SURF_AND_KAYAK'
# CAT_MAP ['surf'] = 'SPORTS_AND_ADVENTURE.SURF_AND_KAYAK'
# CAT_MAP ['windsurf'] = 'SPORTS_AND_ADVENTURE.SURF_AND_KAYAK'
# CAT_MAP ['bicicletas'] = 'SPORTS_AND_ADVENTURE.BIKES'
# CAT_MAP ['bicicletas-02'] = 'SPORTS_AND_ADVENTURE.BIKES'
# CAT_MAP ['bicicletas-03'] = 'SPORTS_AND_ADVENTURE.BIKES'
CAT_MAP ['esqui'] = 'SPORTS_AND_ADVENTURE.SKI'
# CAT_MAP ['caza'] = 'SPORTS_AND_ADVENTURE.HUNTING'
# CAT_MAP ['pesca'] = 'SPORTS_AND_ADVENTURE.FISHING'
CAT_MAP ['raqueta'] = 'SPORTS_AND_ADVENTURE.RACQUET_SPORTS'
# CAT_MAP ['baloncesto'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
# CAT_MAP ['futbol'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
# CAT_MAP ['gimnasios'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
# CAT_MAP ['running'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
# CAT_MAP ['volley'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
# CAT_MAP ['golf'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
# CAT_MAP ['escalada'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
# CAT_MAP ['otros-caballos'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
# CAT_MAP ['monturas-de-caballo'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
# CAT_MAP ['camping'] = 'SPORTS_AND_ADVENTURE.HIKING_AND_OUTDOORS'
# CAT_MAP ['montanismo'] = 'SPORTS_AND_ADVENTURE.HIKING_AND_OUTDOORS'
# CAT_MAP ['deportes'] = 'SPORTS_AND_ADVENTURE'
# 
CAT_MAP ['maquinaria-agricola-de-segunda-mano-toda-espana'] = 'TOOLS_AND_MACHINERY.AGRICULTURE'
# CAT_MAP ['reformas'] = 'TOOLS_AND_MACHINERY.CONSTRUCTION'
# CAT_MAP ['construccion'] = 'TOOLS_AND_MACHINERY.CONSTRUCTION'
# CAT_MAP ['bricolaje'] = 'TOOLS_AND_MACHINERY.DYI'
# CAT_MAP ['bricolaje-02'] = 'TOOLS_AND_MACHINERY.DYI'
# 
# CAT_MAP ['MOTOR'] = 'VEHICLES'
CAT_MAP ['coches-de-segunda-mano-toda-espana'] = 'VEHICLES.CARS_AND_VANS'
# CAT_MAP ['todoterreno-de-ocasion'] = 'VEHICLES.CARS_AND_VANS'
CAT_MAP ['furgonetas-de-segunda-mano-toda-espana'] = 'VEHICLES.CARS_AND_VANS'
CAT_MAP ['autocaravanas-de-segunda-mano-toda-espana'] = 'VEHICLES.CARAVANS'
CAT_MAP ['caravanas-de-segunda-mano-toda-espana'] = 'VEHICLES.CARAVANS'
CAT_MAP ['quads-de-segunda-mano-toda-espana'] = 'VEHICLES.QUADS'
# CAT_MAP ['camiones-usados'] = 'VEHICLES.TRUCKS'
# CAT_MAP ['van-de-caballos'] = 'VEHICLES.TRANSPORT_ACCESSORIES'
# CAT_MAP ['transporte-de-caballos'] = 'VEHICLES.TRANSPORT_ACCESSORIES'
CAT_MAP ['remolques-de-segunda-mano-toda-espana'] = 'VEHICLES.TRANSPORT_ACCESSORIES'
CAT_MAP ['motos-de-segunda-mano-toda-espana'] = 'VEHICLES.MOTORBIKES'
# CAT_MAP ['barcos'] = 'VEHICLES.BOAT'
# CAT_MAP ['motos-de-agua'] = 'VEHICLES.JET_SKI'
# CAT_MAP ['motos-de-agua-02'] = 'VEHICLES.JET_SKI'
# CAT_MAP ['punsair'] = 'VEHICLES.AIRCRAFT'
# CAT_MAP ['volar'] = 'VEHICLES.AIRCRAFT'
# CAT_MAP ['volar-02'] = 'VEHICLES.AIRCRAFT'
# CAT_MAP ['helicoptero'] = 'VEHICLES.AIRCRAFT'
# 
# CAT_MAP ['moda-mujer-ropa'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
# CAT_MAP ['moda-hombre-ropa'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
# CAT_MAP ['calzado'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
# CAT_MAP ['calzado-para-caballero'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
# CAT_MAP ['ropa-infantil-nina'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
# CAT_MAP ['calzado-infantil-nina'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
# CAT_MAP ['ropa-infantil'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
# CAT_MAP ['calzado-infantil'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
# CAT_MAP ['bolsos'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
# CAT_MAP ['cinturones'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
# CAT_MAP ['accesorios-mujer'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
# CAT_MAP ['gafas'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
# CAT_MAP ['cinturones-hombre'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
# CAT_MAP ['gafas-hombre'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
# CAT_MAP ['accesorios-hombre'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
# CAT_MAP ['relojes-hombre'] = 'FASHION_BEAUTY_AND_HEALTH.WATCHES'
# CAT_MAP ['relojes'] = 'FASHION_BEAUTY_AND_HEALTH.WATCHES'
# CAT_MAP ['joyas-hombre'] = 'FASHION_BEAUTY_AND_HEALTH.JEWELRY'
# CAT_MAP ['joyas'] = 'FASHION_BEAUTY_AND_HEALTH.JEWELRY'
# 
# CAT_MAP ['belleza'] = 'FASHION_BEAUTY_AND_HEALTH.BEAUTY'
# CAT_MAP ['belleza-estetica'] = 'FASHION_BEAUTY_AND_HEALTH.BEAUTY'
# CAT_MAP ['belleza-estetica-02'] = 'FASHION_BEAUTY_AND_HEALTH.BEAUTY'
# CAT_MAP ['otros-electrodomesticos-belleza'] = 'FASHION_BEAUTY_AND_HEALTH.BEAUTY'
# CAT_MAP ['solarium'] = 'FASHION_BEAUTY_AND_HEALTH.BEAUTY'
# 
# CAT_MAP ['ortopedia'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'
# CAT_MAP ['ortopedia-02'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'
# CAT_MAP ['dietetica'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'
# CAT_MAP ['dietetica-02'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'
# CAT_MAP ['mobiliario-clinico'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'
# CAT_MAP ['mobiliario-clinico-02'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'
# 
# 
# CAT_MAP ['trasteros'] = 'SPACES.STORAGE_ROOMS'
# CAT_MAP ['trasteros-02'] = 'SPACES.STORAGE_ROOMS'
# 
# CAT_MAP ['viviendas'] = 'SPACES.HOMES_AND_ROOMS'
# CAT_MAP ['viviendas-02'] = 'SPACES.HOMES_AND_ROOMS'
# CAT_MAP ['alquiler-de-viviendas'] = 'SPACES.HOMES_AND_ROOMS'
# CAT_MAP ['alquiler-de-viviendas-02'] = 'SPACES.HOMES_AND_ROOMS'
# CAT_MAP ['pisos-compartidos'] = 'SPACES.HOMES_AND_ROOMS'
# 
# CAT_MAP ['oficinas'] = 'SPACES.OFFICES'
# CAT_MAP ['oficinas-02'] = 'SPACES.OFFICES'
# CAT_MAP ['locales-comerciales'] = 'SPACES.OFFICES'
# #CAT_MAP [''] = 'SPACES.GARDENS_AND_OUTDOORS'
# CAT_MAP ['plazas-de-garaje'] = 'SPACES.GARAGE'
# CAT_MAP ['plazas-de-garaje-02'] = 'SPACES.GARAGE'

print str(len(CAT_MAP)) + " files"

def scrap_file():
    
    # scrap categories
    categories_data = []
    #for category in categories_selection:
    for category in CAT_MAP.keys():
        print category
        category_path = BASE_PATH + category + '.html'
        
        category_raw_html = open(category_path).read()
        category_html = BeautifulSoup(category_raw_html, 'html.parser')
 
        titles = []
        for a in category_html.select('a'):
            if a.get('class',[''])[0].lower() == 'subjecttitle':
            
                title = a.text
                data  = strip_accents(title)
                
                if len(data)>1:
                    #data = category.replace('-',' ') + u' ' + data
                    categories_data += [CAT_MAP.get(category, "NONE") + '\t' + data ]

        for meta in category_html.select('meta'):
            if meta.get('name','') == 'description':
                data = strip_accents(meta.get('content',''))
                if len(data)>1:
                    #categories_data += [CAT_MAP.get(category, "NONE") + '\t' + data]        
                    pass
    
    output = open(OUTPUT_FILE, "w") 
    output.write("\n".join(categories_data)) 
    print "written " + str(len(categories_data)) + " lines"
    
    return
# ---------
def usage():
    print 'Usage: cat_scraper_vibbo.py'

def main(argv):

    try:
        opts, args = getopt.getopt(argv,"h:",["help"])
    except getopt.GetoptError as e:
        print(e)
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        else:
            usage()
            sys.exit(2)
    scrap_file()
    
if __name__ == "__main__":
    main(sys.argv[1:])