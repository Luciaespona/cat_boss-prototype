# encoding=utf8  

import sys, getopt
import time
import collections

from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup

from cat_util import strip_accents, robust_decode

BASE_URL = 'https://www.milanuncios.com/' 
BASE_PATH = '../data/cat_scraper_data/milanuncios/'
#OUTPUT_FILE = '../data/cat_scraper_data/output/milanuncios_scrap_titles_only.csv'
OUTPUT_FILE = '../data/cat_scraper_data/output/milanuncios_scrap.csv'

reload(sys)  
sys.setdefaultencoding('utf8')

CAT_MAP = collections.OrderedDict()
CAT_MAP ['telefonos-moviles'] = 'ELECTRONIC_AND_COMPUTERS.SMARTPHONE'
CAT_MAP ['telefonos-moviles-02'] = 'ELECTRONIC_AND_COMPUTERS.SMARTPHONE'
CAT_MAP ['telefonos-moviles-03'] = 'ELECTRONIC_AND_COMPUTERS.SMARTPHONE'
CAT_MAP ['telefonos-moviles-04'] = 'ELECTRONIC_AND_COMPUTERS.SMARTPHONE'
CAT_MAP ['telefonos-moviles-05'] = 'ELECTRONIC_AND_COMPUTERS.SMARTPHONE'
CAT_MAP ['telefonos-moviles-06'] = 'ELECTRONIC_AND_COMPUTERS.SMARTPHONE'
CAT_MAP ['telefonos-moviles-07'] = 'ELECTRONIC_AND_COMPUTERS.SMARTPHONE'
CAT_MAP ['portatiles-de-segunda-mano'] = 'ELECTRONIC_AND_COMPUTERS.LAPTOPS'
CAT_MAP ['portatiles-de-segunda-mano-02'] = 'ELECTRONIC_AND_COMPUTERS.LAPTOPS'
CAT_MAP ['ordenadores-de-segunda-mano'] = 'ELECTRONIC_AND_COMPUTERS.DESKTOPS'
CAT_MAP ['ordenadores-de-segunda-mano-02'] = 'ELECTRONIC_AND_COMPUTERS.DESKTOPS'
CAT_MAP ['ordenadores-de-segunda-mano-03'] = 'ELECTRONIC_AND_COMPUTERS.DESKTOPS'
CAT_MAP ['tablets'] = 'ELECTRONIC_AND_COMPUTERS.TABLETS'
CAT_MAP ['impresoras'] = 'ELECTRONIC_AND_COMPUTERS.PRINT_AND_SCAN'
CAT_MAP ['fotocopiadoras'] = 'ELECTRONIC_AND_COMPUTERS.PRINT_AND_SCAN'
CAT_MAP ['accesorios-perifericos'] = 'ELECTRONIC_AND_COMPUTERS.ELECTRONIC_ACCESSORIES'
CAT_MAP ['consolas'] = 'ELECTRONIC_AND_COMPUTERS.CONSOLES'
CAT_MAP ['consolas-02'] = 'ELECTRONIC_AND_COMPUTERS.CONSOLES'
CAT_MAP ['videojuegos'] = 'ELECTRONIC_AND_COMPUTERS.VIDEOGAMES'
CAT_MAP ['videojuegos-02'] = 'ELECTRONIC_AND_COMPUTERS.VIDEOGAMES'
CAT_MAP ['router'] = 'ELECTRONIC_AND_COMPUTERS.NETWORKING'
CAT_MAP ['ruter'] = 'ELECTRONIC_AND_COMPUTERS.NETWORKING'
CAT_MAP ['switch'] = 'ELECTRONIC_AND_COMPUTERS.NETWORKING'
CAT_MAP ['ethernet'] = 'ELECTRONIC_AND_COMPUTERS.NETWORKING'
CAT_MAP ['dron'] = 'ELECTRONIC_AND_COMPUTERS.DRONES'
CAT_MAP ['dron-02'] = 'ELECTRONIC_AND_COMPUTERS.DRONES'

CAT_MAP ['videocamaras'] = 'AUDIO_VISUAL.FILM'
CAT_MAP ['videocamaras-02'] = 'AUDIO_VISUAL.FILM'
CAT_MAP ['videocamaras-03'] = 'AUDIO_VISUAL.FILM'
CAT_MAP ['videocamaras-04'] = 'AUDIO_VISUAL.FILM'
CAT_MAP ['fotografia'] = 'AUDIO_VISUAL.PHOTOGRAPHY'
CAT_MAP ['fotografia-02'] = 'AUDIO_VISUAL.PHOTOGRAPHY'
CAT_MAP ['fotografia-03'] = 'AUDIO_VISUAL.PHOTOGRAPHY'
CAT_MAP ['sonido'] = 'AUDIO_VISUAL.AUDIO_AND_DJ_EQUIPMENT'
CAT_MAP ['sonido-02'] = 'AUDIO_VISUAL.AUDIO_AND_DJ_EQUIPMENT'
CAT_MAP ['sonido-03'] = 'AUDIO_VISUAL.AUDIO_AND_DJ_EQUIPMENT'
CAT_MAP ['mesas-de-mezclas'] = 'AUDIO_VISUAL.AUDIO_AND_DJ_EQUIPMENT'
CAT_MAP ['videoproyectores'] = 'AUDIO_VISUAL.PROJECTORS'
CAT_MAP ['videoproyectores-02'] = 'AUDIO_VISUAL.PROJECTORS'
CAT_MAP ['videoproyectores-03'] = 'AUDIO_VISUAL.PROJECTORS'
CAT_MAP ['televisores'] = 'AUDIO_VISUAL.SCREENS'
CAT_MAP ['televisores-02'] = 'AUDIO_VISUAL.SCREENS'
CAT_MAP ['televisores-03'] = 'AUDIO_VISUAL.SCREENS'

CAT_MAP ['instrumentos-musicales'] = 'ARTS_AND_EDUCATION.MUSICAL_INSTRUMENTS'
CAT_MAP ['instrumentos-musicales-02'] = 'ARTS_AND_EDUCATION.MUSICAL_INSTRUMENTS'
CAT_MAP ['instrumentos-musicales-03'] = 'ARTS_AND_EDUCATION.MUSICAL_INSTRUMENTS'
CAT_MAP ['instrumentos-musicales-04'] = 'ARTS_AND_EDUCATION.MUSICAL_INSTRUMENTS'
CAT_MAP ['formacion'] = 'ARTS_AND_EDUCATION.LEARNING_MATERIAL'
CAT_MAP ['pinturas'] = 'ARTS_AND_EDUCATION.ART_AND_CRAFTS'
CAT_MAP ['esculturas'] = 'ARTS_AND_EDUCATION.ART_AND_CRAFTS'
CAT_MAP ['otros-articulos-de-arte'] = 'ARTS_AND_EDUCATION.ART_AND_CRAFTS'
CAT_MAP ['libros'] = 'ARTS_AND_EDUCATION.CINEMA_MUSIC_AND_LITERATURE'
CAT_MAP ['peliculas'] = 'ARTS_AND_EDUCATION.CINEMA_MUSIC_AND_LITERATURE'
CAT_MAP ['peliculas-02'] = 'ARTS_AND_EDUCATION.CINEMA_MUSIC_AND_LITERATURE'
CAT_MAP ['peliculas-03'] = 'ARTS_AND_EDUCATION.CINEMA_MUSIC_AND_LITERATURE'
CAT_MAP ['cds-cintas-y-vinilos'] = 'ARTS_AND_EDUCATION.CINEMA_MUSIC_AND_LITERATURE'
CAT_MAP ['cds'] = 'ARTS_AND_EDUCATION.CINEMA_MUSIC_AND_LITERATURE'

CAT_MAP ['cunas-para-bebe'] = 'BABYS_AND_KIDS.BABY_CARE'
CAT_MAP ['tronas'] = 'BABYS_AND_KIDS.BABY_CARE'
CAT_MAP ['baneras-cambiadores'] = 'BABYS_AND_KIDS.BABY_CARE'
CAT_MAP ['accesorios-para-bebe'] = 'BABYS_AND_KIDS.BABY_CARE'
CAT_MAP ['hamacas-columpios'] = 'BABYS_AND_KIDS.BABY_CARE'
CAT_MAP ['mochilas-fulares-portabebes'] = 'BABYS_AND_KIDS.PRAMS_AND_CARRIERS'
CAT_MAP ['coches-de-bebe'] = 'BABYS_AND_KIDS.PRAMS_AND_CARRIERS'
CAT_MAP ['silla-infantil'] = 'BABYS_AND_KIDS.CAR_AND_BIKE_SEATS'
CAT_MAP ['juguetes-para-bebes'] = 'BABYS_AND_KIDS.TOYS_AND_GAMES'
CAT_MAP ['juguetes'] = 'BABYS_AND_KIDS.TOYS_AND_GAMES'
CAT_MAP ['juguetes-02'] = 'BABYS_AND_KIDS.TOYS_AND_GAMES'
CAT_MAP ['juguetes-03'] = 'BABYS_AND_KIDS.TOYS_AND_GAMES'

CAT_MAP ['disfraces'] = 'PARTY_AND_EVENTS.PARTY_COSTUMES'
CAT_MAP ['disfraces-hombre'] = 'PARTY_AND_EVENTS.PARTY_COSTUMES'
CAT_MAP ['disfraces-nino'] = 'PARTY_AND_EVENTS.PARTY_COSTUMES'
CAT_MAP ['organizacion-de-fiestas'] = 'PARTY_AND_EVENTS.PARTY_DECORATION'
CAT_MAP ['celebraciones-local'] = 'PARTY_AND_EVENTS.PARTY_SPACES'

CAT_MAP ['piscinas'] = 'HOME_AND_GARDEN.POOL'
CAT_MAP ['cortacesped'] = 'HOME_AND_GARDEN.GARDEN'
CAT_MAP ['herramientas-de-jardin'] = 'HOME_AND_GARDEN.GARDEN'
CAT_MAP ['plantas'] = 'HOME_AND_GARDEN.GARDEN'
CAT_MAP ['mobiliario-de-jardin'] = 'HOME_AND_GARDEN.GARDEN'
CAT_MAP ['electrodomesticos'] = 'HOME_AND_GARDEN.ELECTRICAL_HOUSEHOLD_APPLIANCE'
CAT_MAP ['iluminacion'] = 'HOME_AND_GARDEN.ILLUMINATION'
CAT_MAP ['arte-y-decoracion'] = 'HOME_AND_GARDEN.DECORATION'
CAT_MAP ['articulos-textiles'] = 'HOME_AND_GARDEN.DECORATION'
CAT_MAP ['menaje-del-hogar'] = 'HOME_AND_GARDEN.DECORATION'
CAT_MAP ['antiguedades-de-coleccion'] = 'HOME_AND_GARDEN.DECORATION'
CAT_MAP ['velas'] = 'HOME_AND_GARDEN.DECORATION'
CAT_MAP ['muebles'] = 'HOME_AND_GARDEN.FURNITURE'
CAT_MAP ['muebles-02'] = 'HOME_AND_GARDEN.FURNITURE'
CAT_MAP ['muebles-03'] = 'HOME_AND_GARDEN.FURNITURE'
CAT_MAP ['accesorios-productos-perros'] = 'HOME_AND_GARDEN.PETS_SUPPLIES'
CAT_MAP ['accesorios-productos-gatos'] = 'HOME_AND_GARDEN.PETS_SUPPLIES'
CAT_MAP ['accesorios-productos-pajaros'] = 'HOME_AND_GARDEN.PETS_SUPPLIES'

CAT_MAP ['kayak'] = 'SPORTS_AND_ADVENTURE.SURF_AND_KAYAK'
CAT_MAP ['surf'] = 'SPORTS_AND_ADVENTURE.SURF_AND_KAYAK'
CAT_MAP ['windsurf'] = 'SPORTS_AND_ADVENTURE.SURF_AND_KAYAK'
CAT_MAP ['bicicletas'] = 'SPORTS_AND_ADVENTURE.BIKES'
CAT_MAP ['bicicletas-02'] = 'SPORTS_AND_ADVENTURE.BIKES'
CAT_MAP ['bicicletas-03'] = 'SPORTS_AND_ADVENTURE.BIKES'
CAT_MAP ['esqui'] = 'SPORTS_AND_ADVENTURE.SKI'
CAT_MAP ['snowboard'] = 'SPORTS_AND_ADVENTURE.SKI'
CAT_MAP ['caza'] = 'SPORTS_AND_ADVENTURE.HUNTING'
CAT_MAP ['pesca'] = 'SPORTS_AND_ADVENTURE.FISHING'
CAT_MAP ['tenis'] = 'SPORTS_AND_ADVENTURE.RACQUET_SPORTS'
CAT_MAP ['pingpong'] = 'SPORTS_AND_ADVENTURE.RACQUET_SPORTS'
CAT_MAP ['padel'] = 'SPORTS_AND_ADVENTURE.RACQUET_SPORTS'
CAT_MAP ['squash'] = 'SPORTS_AND_ADVENTURE.RACQUET_SPORTS'
CAT_MAP ['baloncesto'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
CAT_MAP ['futbol'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
CAT_MAP ['gimnasios'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
CAT_MAP ['running'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
CAT_MAP ['volley'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
CAT_MAP ['golf'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
CAT_MAP ['escalada'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
CAT_MAP ['otros-caballos'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
CAT_MAP ['monturas-de-caballo'] = 'SPORTS_AND_ADVENTURE.OTHER_SPORTS'
CAT_MAP ['camping'] = 'SPORTS_AND_ADVENTURE.HIKING_AND_OUTDOORS'
CAT_MAP ['montanismo'] = 'SPORTS_AND_ADVENTURE.HIKING_AND_OUTDOORS'
CAT_MAP ['deportes'] = 'SPORTS_AND_ADVENTURE'

CAT_MAP ['maquinaria-agricola'] = 'TOOLS_AND_MACHINERY.AGRICULTURE'
CAT_MAP ['maquinaria-agricola-02'] = 'TOOLS_AND_MACHINERY.AGRICULTURE'
CAT_MAP ['maquinaria-agricola-03'] = 'TOOLS_AND_MACHINERY.AGRICULTURE'
CAT_MAP ['maquinaria-agricola-04'] = 'TOOLS_AND_MACHINERY.AGRICULTURE'
CAT_MAP ['maquinaria-agricola-05'] = 'TOOLS_AND_MACHINERY.AGRICULTURE'
CAT_MAP ['reformas'] = 'TOOLS_AND_MACHINERY.CONSTRUCTION'
CAT_MAP ['construccion'] = 'TOOLS_AND_MACHINERY.CONSTRUCTION'
CAT_MAP ['bricolaje'] = 'TOOLS_AND_MACHINERY.DYI'
CAT_MAP ['bricolaje-02'] = 'TOOLS_AND_MACHINERY.DYI'

CAT_MAP ['MOTOR'] = 'VEHICLES'
CAT_MAP ['coches-de-segunda-mano'] = 'VEHICLES.CARS_AND_VANS'
CAT_MAP ['coches-de-segunda-mano-02'] = 'VEHICLES.CARS_AND_VANS'
CAT_MAP ['coches-de-segunda-mano-03'] = 'VEHICLES.CARS_AND_VANS'
CAT_MAP ['todoterreno-de-ocasion'] = 'VEHICLES.CARS_AND_VANS'
CAT_MAP ['furgonetas-de-segunda-mano'] = 'VEHICLES.CARS_AND_VANS'
CAT_MAP ['autocaravanas'] = 'VEHICLES.CARAVANS'
CAT_MAP ['autocaravana-camperizada'] = 'VEHICLES.CARAVANS'
CAT_MAP ['caravanas-de-segunda-mano'] = 'VEHICLES.CARAVANS'
CAT_MAP ['quads'] = 'VEHICLES.QUADS'
CAT_MAP ['camiones-usados'] = 'VEHICLES.TRUCKS'
CAT_MAP ['van-de-caballos'] = 'VEHICLES.TRANSPORT_ACCESSORIES'
CAT_MAP ['transporte-de-caballos'] = 'VEHICLES.TRANSPORT_ACCESSORIES'
CAT_MAP ['remolques'] = 'VEHICLES.TRANSPORT_ACCESSORIES'
CAT_MAP ['motos-de-segunda-mano'] = 'VEHICLES.MOTORBIKES'
CAT_MAP ['motos-de-segunda-mano-02'] = 'VEHICLES.MOTORBIKES'
CAT_MAP ['motos-de-segunda-mano-03'] = 'VEHICLES.MOTORBIKES'
CAT_MAP ['barcos'] = 'VEHICLES.BOAT'
CAT_MAP ['motos-de-agua'] = 'VEHICLES.JET_SKI'
CAT_MAP ['motos-de-agua-02'] = 'VEHICLES.JET_SKI'
CAT_MAP ['punsair'] = 'VEHICLES.AIRCRAFT'
CAT_MAP ['volar'] = 'VEHICLES.AIRCRAFT'
CAT_MAP ['volar-02'] = 'VEHICLES.AIRCRAFT'
CAT_MAP ['helicoptero'] = 'VEHICLES.AIRCRAFT'

CAT_MAP ['moda-mujer-ropa'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
CAT_MAP ['moda-hombre-ropa'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
CAT_MAP ['calzado'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
CAT_MAP ['calzado-para-caballero'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
CAT_MAP ['ropa-infantil-nina'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
CAT_MAP ['calzado-infantil-nina'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
CAT_MAP ['ropa-infantil'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
CAT_MAP ['calzado-infantil'] = 'FASHION_BEAUTY_AND_HEALTH.CLOTHES_AND_SHOES'
CAT_MAP ['bolsos'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
CAT_MAP ['cinturones'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
CAT_MAP ['accesorios-mujer'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
CAT_MAP ['gafas'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
CAT_MAP ['cinturones-hombre'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
CAT_MAP ['gafas-hombre'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
CAT_MAP ['accesorios-hombre'] = 'FASHION_BEAUTY_AND_HEALTH.FASHION_ACCESSORIES'
CAT_MAP ['relojes-hombre'] = 'FASHION_BEAUTY_AND_HEALTH.WATCHES'
CAT_MAP ['relojes'] = 'FASHION_BEAUTY_AND_HEALTH.WATCHES'
CAT_MAP ['joyas-hombre'] = 'FASHION_BEAUTY_AND_HEALTH.JEWELRY'
CAT_MAP ['joyas'] = 'FASHION_BEAUTY_AND_HEALTH.JEWELRY'
CAT_MAP ['joyas-02'] = 'FASHION_BEAUTY_AND_HEALTH.JEWELRY'


CAT_MAP ['belleza'] = 'FASHION_BEAUTY_AND_HEALTH.BEAUTY'
CAT_MAP ['belleza-estetica'] = 'FASHION_BEAUTY_AND_HEALTH.BEAUTY'
CAT_MAP ['belleza-estetica-02'] = 'FASHION_BEAUTY_AND_HEALTH.BEAUTY'
CAT_MAP ['otros-electrodomesticos-belleza'] = 'FASHION_BEAUTY_AND_HEALTH.BEAUTY'
CAT_MAP ['solarium'] = 'FASHION_BEAUTY_AND_HEALTH.BEAUTY'
CAT_MAP ['ortopedia'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'
CAT_MAP ['ortopedia-02'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'
CAT_MAP ['dietetica'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'
CAT_MAP ['dietetica-02'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'
CAT_MAP ['mobiliario-clinico'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'
CAT_MAP ['mobiliario-clinico-02'] = 'FASHION_BEAUTY_AND_HEALTH.HEALTH_CARE'


CAT_MAP ['trasteros'] = 'SPACES.STORAGE_ROOMS'
CAT_MAP ['trasteros-02'] = 'SPACES.STORAGE_ROOMS'

CAT_MAP ['viviendas'] = 'SPACES.HOMES_AND_ROOMS'
CAT_MAP ['viviendas-02'] = 'SPACES.HOMES_AND_ROOMS'
CAT_MAP ['alquiler-de-viviendas'] = 'SPACES.HOMES_AND_ROOMS'
CAT_MAP ['alquiler-de-viviendas-02'] = 'SPACES.HOMES_AND_ROOMS'
CAT_MAP ['pisos-compartidos'] = 'SPACES.HOMES_AND_ROOMS'

CAT_MAP ['oficinas'] = 'SPACES.OFFICES'
CAT_MAP ['oficinas-02'] = 'SPACES.OFFICES'
CAT_MAP ['locales-comerciales'] = 'SPACES.OFFICES'
#CAT_MAP [''] = 'SPACES.GARDENS_AND_OUTDOORS'
CAT_MAP ['plazas-de-garaje'] = 'SPACES.GARAGE'
CAT_MAP ['plazas-de-garaje-02'] = 'SPACES.GARAGE'

print str(len(CAT_MAP)) + " files"

def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:62.0) Gecko/20100101 Firefox/62.0'}

    try:
        with closing(get(url, headers=headers, stream=True, timeout=5)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200 
            and content_type is not None 
            and content_type.find('html') > -1)


def log_error(e):
    """
    It is always a good idea to log errors. 
    This function just prints them, but you can
    make it do anything.
    """
    print(e)

# -----------------
def scrap_url(url):
    raw_html = simple_get(url)
    return raw_html    

def scrap_file(file):

    # get categories from index.html
    raw_html = open(file).read()
    html = BeautifulSoup(raw_html, 'html.parser')
    categories = []
    
    for a in html.select('a'):
        if a.get('class',[''])[0] == 'cat1':
            section = a.get('href','').upper().replace('/','')
            categories += [ section ]
        elif a.get('class',[''])[0] == 'cat2':
            category = a.get('href','').lower().replace('/','')
            categories += [ category ]
    #print ', '.join(categories)
    
    #categories_selection = list(set(categories + [ 'autocaravanas' ]))
    
    # scrap categories
    categories_data = []
    #for category in categories_selection:
    for category in CAT_MAP.keys():
        print(category)
        category_path = BASE_PATH + category + '.html'
        
        category_raw_html = open(category_path).read()
        category_html = BeautifulSoup(category_raw_html, 'html.parser')
 
        titles = []
        for a in category_html.select('a'):
            if a.get('class',[''])[0] == 'aditem-detail-title':
            
                title = a.text
                data  = strip_accents(title)
                
                if len(data)>1:
                    data = category.replace('-',' ') + u' ' + data
                
                    categories_data += [CAT_MAP.get(category, "NONE") + '\t' + data ]
 
        for div in category_html.select('div'):
            if div.get('class',[''])[0] == 'tx':
                data = strip_accents(div.text)
                if len(data)>1:
                    categories_data += [CAT_MAP.get(category, "NONE") + '\t' + data]
                    pass

        for meta in category_html.select('meta'):
            if meta.get('name','') == 'description':
                data = strip_accents(meta.get('content',''))
                if len(data)>1:
                    categories_data += [CAT_MAP.get(category, "NONE") + '\t' + data]        
                    pass
    
    output = open(OUTPUT_FILE, "w") 
    output.write("\n".join(categories_data)) 
    print "written " + str(len(categories_data)) + " lines"
    
    return
# ---------
def usage():
    print 'Usage: cat_scraper_milanuncios.py -f <file>'

def main(argv):

    file = ''
    try:
        opts, args = getopt.getopt(argv,"hf:",["file=", "help"])
    except getopt.GetoptError as e:
        print(e)
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-f", "--file"):
            file = arg.strip()
        else:
            usage()
            sys.exit(2)
    if file:
        print (' - file is "' + file + '"')
        scrap_file(file)
    else:
        usage()

    
if __name__ == "__main__":
    main(sys.argv[1:])