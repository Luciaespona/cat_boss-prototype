# encoding=utf8  

import cat_boss
import cat_data

#TEST_FILE_PATHS = ['../data/cat_scraper_data/output/milanuncios_scrap_titles_only.csv']
TEST_FILE_PATHS = ['../data/cat_scraper_data/output/vibbo_scrap_curated.csv']
OUTPUT_FILE = '../data/cat_tester_output.txt'

def add_to_fail(fail_report, section, category, score=1):

    if not fail_report.get(section, False):
        fail_report[section] = 0
    fail_report[section] += score

    category_key = section+'.'+category
    if not fail_report.get(category_key, False):
        fail_report[category_key] = 0
    fail_report[category_key] += score

    return


def main():
    
    bad_cat = 0
    bad_sec = 0
    bad_only_stopwords = 0
    first_or_second_section = 0
    
    fail_report = {}
    
    cat_db = cat_data.init_cat_db()

    for file_test in TEST_FILE_PATHS:
        print " testing on file " + file_test
        line_num = 0
        with open(file_test) as f:
            for line in f.readlines():
                line_num += 1
                if len(line.strip())>0:             
                    category_text = unicode(line.strip().split('\t')[0].upper())
                    try:
                        text = line.strip().split('\t')[1].upper()
                    except:
                        print 'ERROR'
                        print text
                        print line_num
                        sys.exit(0)
                
                    section = category_text.split('.')[0].upper()
                
                    if len(category_text.split('.'))>1:
                        category = category_text.split('.')[1].upper()
                    else:
                        category = ""
                    
                    #print '\n' + str(line_num) + ': ' + section + '.' + category 
                    #print text
                    sections, categories = cat_boss.suggest_category(text, verbose=False, db = cat_db)
                    #print '\t' + str(sections) + '\t' + str(categories) 
                    
                    if section in sections.keys():
                        #print " SECTION OK "
                        if sections.keys().index(section)<2:
                            first_or_second_section += 1
                        else:
                            print "\n * Not 1st/2nd: " + section + '.' + category + ' (' + str(line_num) + '):' + text
                            add_to_fail(fail_report, section, category, score=0.5)
                        
                        if category and len(categories)>0:
                            if section + '.' + category in categories.keys():
                                #print " CAT OK "
                                pass
                            else:
                                bad_cat += 1
                                print "\n * CAT WRONG * (" + str(line_num) + " )" + category + ' not in ' + str(categories.keys())
                                print text
                                add_to_fail(fail_report, section, category)
                    else:
                        bad_sec += 1
                        print "\n * SECTION WRONG * (" + str(line_num) + " ) " + section + ' not in ' + str(sections.keys())
                        print " * CAT WRONG * (" + str(line_num) + " )" + category + ' not in ' + str(categories.keys())
                        print text
                        add_to_fail(fail_report, section, category, score = 2)
                        
                        if len(sections.keys()) == 0:
                            bad_only_stopwords += 1


                    
    print " total = " + str(line_num)
    print " first_or_second_section = " + str(first_or_second_section)
    print " bad_sec = " + str(bad_sec)
    print " bad_cat = " + str(bad_cat)
    print " bad_only_stopwords = " + str(bad_only_stopwords)
    
    fail_report_sort = fail_report.keys()
    fail_report_sort.sort()
    for key in fail_report_sort:
        print key + '\t ' + str(fail_report[key])
    
    print " -> Close DB"
    cat_data.close_db(cat_db)                
  
if __name__ == "__main__":
    main()