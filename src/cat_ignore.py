import cat_util

STOPWORDS_FILES = ['../data/stopwords-es/stopwords-es.txt', '../data/propio_stopwords.txt']
STOPWORDS_SCRAP_FILES = [ '../data/milanuncios_stopwords.txt']

def get_stopwords(scrap=False):
    stopwords = []
    for file_name in STOPWORDS_FILES:
        with open(file_name) as f:
            for line in f.readlines():
                word = unicode(line.strip())
                word = cat_util.strip_accents(word).upper()
                stopwords += [word]
    if scrap:
        for file_name in STOPWORDS_SCRAP_FILES:
            with open(file_name) as f:
                for line in f.readlines():
                    word = unicode(line.strip())
                    word = cat_util.strip_accents(word).upper()
                    stopwords += [word]
    
    return list(set(stopwords))
