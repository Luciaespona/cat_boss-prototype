import sqlite3

DB_PATH = '../data/sqlite_db/cat_boss_db'
CAT_FILE_PATH = '../data/sections_categories.csv'
TOKEN_FILE_PATH = '../data/cat_miner_output/miner_output.csv'
CATEGORY_TB = 'category'
TOKEN_TB = 'token'

def table_exists(db, table_name):
    # Get a cursor object
    cursor = db.cursor()
    cursor.execute('''
        SELECT name FROM sqlite_master WHERE type='table' AND name=?
        ''', (table_name,))
    all_rows = cursor.fetchall()
    return len(all_rows)>0

def table_is_empty(db, table_name):
    # Get a cursor object
    cursor = db.cursor()
    cursor.execute(''' SELECT * FROM ''' + table_name )
    all_rows = cursor.fetchall()
    return len(all_rows)<=0


def init_cat_db():
    #print "init_db " + DB_PATH
    db = sqlite3.connect(DB_PATH)
    cursor = db.cursor()
    
    # create CATEGORY table
    if not table_exists(db, CATEGORY_TB):
        print("create category table")
        cursor.execute('''
                         CREATE TABLE ''' +  CATEGORY_TB + ''' (
                           id INTEGER PRIMARY KEY, 
                           section TEXT,
                           category TEXT unique)
                        ''')
        db.commit()

    # fill up category table
    if table_is_empty(db, CATEGORY_TB):
        print("fill up category table")
        with open(CAT_FILE_PATH) as f:
            index = 0
            for line in f.readlines():
                index += 1
                section = line.strip().split()[0].upper()
                category = line.strip().split()[1].upper()
                cursor.execute('''INSERT INTO ''' + CATEGORY_TB + '''(id, section, category)
                  VALUES(:index,:section, :category)''',
                  {'index':index, 'section':section, 'category':category})
        db.commit()
        
    
    # create TOKEN table
    if not table_exists(db, TOKEN_TB):
        print("create token table")
        cursor.execute('''
                         CREATE TABLE ''' +  TOKEN_TB + ''' (
                           id INTEGER PRIMARY KEY, 
                           token TEXT, 
                           section TEXT NOT NULL,
                           category TEXT,
                           score INTEGER)
                        ''')
        db.commit()

    # fill up token table
    if table_is_empty(db, TOKEN_TB):
        print("fill up token table")
        with open(TOKEN_FILE_PATH) as f:
            index = 0
            for line in f.readlines():
                if len(line.strip())>0:
                    index += 1
                    
                    token = unicode(line.strip().split()[0].upper())
                    section = line.strip().split()[1].upper()
                    
                    try:
                        category = line.strip().split()[2].upper()
                    except:
                        #print section
                        category = None
                    
                    score = 0
                    
                    cursor.execute('''INSERT INTO ''' + TOKEN_TB + '''(id, token, section, category, score)
                                      VALUES(:id, :token,:section, :category, :score)''',
                                   {'id':index, 'token':token, 'section':section, 'category':category, 'score':score})
        db.commit()
        db_stats(db)

    return db

def db_stats(db):
    cursor = db.cursor()
    
    cursor.execute('''SELECT * FROM ''' + CATEGORY_TB )
    all_rows = cursor.fetchall()
    print("table '" + CATEGORY_TB + "' contains " + str(len(all_rows)) + " row(s) \n")

    cursor.execute('''SELECT * FROM ''' + TOKEN_TB )
    all_rows = cursor.fetchall()
    print("Table '" + TOKEN_TB + "' contains " + str(len(all_rows)) + " row(s)")
    
    print "\n * Categories: "
    cursor.execute('''SELECT SECTION, COUNT(*) as tokens FROM ''' + TOKEN_TB + ''' GROUP BY SECTION ORDER BY tokens''')
    for row in cursor.fetchall():
        print ' - ' + row[0] + '\t' + str(row[1])
    
    print "\n * Sections: "
    cursor.execute('''SELECT section, category, COUNT(*) AS tokens FROM ''' + TOKEN_TB + 
                   ''' WHERE section IS NOT NULL
                      GROUP BY section, category
                      ORDER BY tokens''')
    for row in cursor.fetchall():
        if row[1]:
            print ' - ' + str(row[1]) + ' (' + str(row[0]) + ')\t' + str(row[2])
    
    return
    
def query_tokens(db, tokens):
    results = {}
    
    for token in tokens:
        cursor = db.cursor()
        cursor.execute('''
            SELECT * FROM ''' + TOKEN_TB + ''' WHERE token=?
              ''', (token,))
        all_rows = cursor.fetchall()
        results[token] = all_rows
    return results

def close_db(db):
    db.close()